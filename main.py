import argparse
from os import path
from rsa import cipher, keymaker


parser = argparse.ArgumentParser(description="RSA key generator and cipher")
parser.add_argument(
    "--generate", "-G", dest="generate", action="store_true",
    help="Generate public and private keys")
parser.add_argument(
    "--complexity", "-K", dest="complexity", type=int,
    help="Number of bits to use for key generator")
parser.add_argument(
    "--public-file", "-P", dest="public_key", type=str,
    help="Path to a public key")
parser.add_argument(
    "--private-file", "-p", dest="private_key", type=str,
    help="Path to a private key")

parser.add_argument(
    "--input", "-i", dest="input", type=str,
    help="Path to source file")
parser.add_argument(
    "--output", "-o", dest="output", type=str,
    help="Path to result file")
parser.add_argument(
    "--encode", "-e", dest="encode",
    action="store_const", const=1,
    help="Encode input using RSA algorithm")
parser.add_argument(
    "--decode", "-d", dest="encode",
    action="store_const", const=-1,
    help="Decode input using RSA algorithm")
parser.add_argument(
    "--sign", "-s", dest="sign", action="store_true", default=False,
    help="Sign encoded message RSA algorithm")
parser.add_argument(
    "--check-sign", dest="signature", type=str,
    help="Check sign")

args = parser.parse_args()

if args.generate:
    keymaker.generate_keys(args.public_key, args.private_key, args.complexity)

elif args.encode is not None:
    if not path.exists(args.input):
        raise "File not exists: %s" % args.input
    result = None
    signature = None
    # Encrypt
    if args.encode == 1:
        if not path.exists(args.public_key):
            raise "Public key not exists: %s" % args.public_key
        with open(args.input, 'rb') as f:
            message = f.read()
            result = cipher.encrypt(message, args.public_key)
            if args.sign:
                if not args.private_key or not path.exists(args.private_key):
                    raise "Private key not exists: %s" % args.private_key
                signature = cipher.sign(result, args.private_key)
    # Decrypt
    elif args.encode == -1:
        if not path.exists(args.private_key):
            raise "Private key not exists: %s" % args.private_key
        with open(args.input, 'rb') as f:
            encrypted_message = f.read()
            if args.signature:
                if cipher.check_sign(encrypted_message, args.signature, args.public_key):
                    print("# Signature validation success!")
                else:
                    raise Exception("# Signature validation error!")
            result = cipher.decrypt(encrypted_message, args.private_key)
    if not args.output:
        print('Encrypted message:', result, sep=' ')
        if signature:
            print('Signature:', signature, sep=' ')
    elif result:
        print("# Saved result at", args.output)
        with open(args.output, 'wb') as w:
            w.write(result)
        if signature:
            print("# Signature saved result at", args.output + '.sig')
            with open(args.output + '.sig', 'wb') as w:
                w.write(signature)
# Check sign
elif args.signature:
    with open(args.input, 'rb') as f:
        encrypted_message = f.read()
        if not path.exists(args.signature):
            raise Exception("Signature file does not exists: %s" % args.signature)
        if cipher.check_sign(encrypted_message, args.signature, args.public_key):
            print("# Signature validation success!")
        else:
            raise Exception("# Signature validation error!")

else:
    parser.print_usage()
