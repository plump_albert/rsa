# RSA encryption algorithm

## ROADMAP

1. Generate 2 prime numbers:
	- [x] Preselect random number with desired bit-size;
	- [x] Ensure it is not divisible by first 100 prime numbers;
	- [x] Apply 20 [Rabin Miller Primality Test](https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test)
	iterations.
