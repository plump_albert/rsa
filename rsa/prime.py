import secrets
import random


def get_prime(bits: int = 256, steps: int = 20) -> int:
    """
    Returns random prime number with `bits` number of random bits

    :param bits int: number of random bits
    :param steps int: number of steps for Miller-Rabin test
    :rtype int: Prime number
    """
    def primes_generator():
        """
        Generates prime numbers

        """
        D = {}
        q = 2
        while True:
            if q not in D:
                yield q
                D[q*q] = [q]
            else:
                for p in D[q]:
                    D.setdefault(p+q, []).append(p)
                del D[q]
            q += 1

    g = primes_generator()
    primes = [next(g) for _ in range(100)]

    def low_prime_number():
        """
        Generates low prime number

        """
        while True:
            candidate = secrets.randbits(bits)
            for p in primes:
                if candidate % p == 0 and p**2 <= candidate:
                    break
                else:
                    return candidate

    while True:
        low_p = low_prime_number()
        if miller_rabin(low_p, steps):
            return low_p


def miller_rabin(candidate: int, steps: int = 20) -> bool:
    """
    Checks if `candidate` is prime number

    :param candidate int: number to check for primarility
    :param steps int: number of steps for Miller-Rabin test
    :rtype bool: `True` if `candidate` is a prime number, `False` otherwise
    """
    s = 0
    d = candidate - 1
    while d % 2 == 0:
        d >>= 1
        s += 1
    # 2^s * d = n
    assert(2**s * d + 1 == candidate)

    def test_composite(a: int):
        if pow(a, 2**s * d, candidate) == 1:
            return False
        for i in range(s):
            if pow(a, 2**i * d, candidate) == candidate - 1:
                return False
        return True

    for _ in range(steps):
        if test_composite(random.randrange(2, candidate)):
            return False
    return True


def extended_gcd(a: int, b: int) -> tuple[int, int, int]:
    """
    Performs extended Euclidian algorithm to find GCD, coefficients for a and b

    :param a int: First number
    :param b int: Second number
    """
    s, old_s = 0, 1
    t, old_t = 1, 0
    while b != 0:
        quotient = a // b
        a, b = b, a - quotient*b
        old_s, s = s, old_s - quotient*s
        old_t, t = t, old_t - quotient*t

    return (a, old_s, old_t)
