import base64
import hashlib
import os


def parse_key(key: bytes) -> tuple[int, int]:
    key_bytes = base64.b64decode(key)

    def read_data(index: int) -> tuple[bytes, int]:
        size = int.from_bytes(
            key_bytes[index: index + 4],
            byteorder='big', signed=False)
        index += 4
        return (key_bytes[index:index + size], index + size)

    i = 0
    # Read exponent
    e, i = read_data(i)
    # Read modulus
    n, i = read_data(i)
    return (
        int.from_bytes(n, byteorder='big'),
        int.from_bytes(e, byteorder='big')
    )


def encrypt(message: bytes, public_key_path: str) -> bytes:
    n, e = parse_key(public_key_path)
    result = bytearray()
    for c in message:
        cipher = pow(c, e, n)
        cipher_length = (cipher.bit_length() + 7) // 8
        # Write cipher length
        result += cipher_length.to_bytes(4, byteorder='big')
        result += cipher.to_bytes(cipher_length, byteorder='big')
    return result


def decrypt(encrypted_message: bytes, private_key: bytes):
    n, d = parse_key(private_key)
    i = 0
    result = bytearray()
    while i < len(encrypted_message):
        size = int.from_bytes(encrypted_message[i:i+4], byteorder='big')
        i += 4
        c = int.from_bytes(encrypted_message[i:i+size], byteorder='big')
        i += size
        msg = pow(c, d, n)
        result.append(msg)
    return result


def sign(encrypted_message: bytes, private_key: bytes):
    message_hash = hashlib.sha512(encrypted_message).digest()
    return encrypt(message_hash, private_key)


def check_sign(message: bytes, signature: bytes, public_key: bytes):
    message_hash = hashlib.sha512(message).digest()
    return decrypt(signature, public_key) == message_hash
