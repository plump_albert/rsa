import base64
from . import prime


def gcd(a: int, b: int) -> int:
    if b == 0:
        return a
    else:
        return gcd(b, a % b)


def generate_keys(
        complexity: int = 1024,
        public_key_path: str = None, private_key_path: str = None
) -> tuple[bytes, bytes]:
    """
    Generates public and private keys for RSA algorithm

    :param public_key str: Path to a file where to save public key
    :param private_key str: Path to a file where to save private key
    :param complexity int: Number of bits to use for key generation
    """
    if not complexity:
        complexity = 1024
    e = 65537
    while True:
        # Generate p and q prime numbers
        p = prime.get_prime(complexity)
        q = prime.get_prime(complexity)
        # Compute n = p*q
        n = p * q
        # Compute λ(n) = (p - 1) * (q - 1)
        L = (p - 1)*(q - 1)
        # if e and λ(n) is coprime -- break
        if gcd(e, L) == 1 and e < L:
            break

    # Determine d ≡ e^(−1) (mod λ(n))
    _, x, _ = prime.extended_gcd(e, L)
    if x < 0:
        d = x + L
    else:
        d = x

    # Write public key
    public_key = bytearray()
    # Write exponent
    e_byte_size = (e.bit_length() + 7) // 8
    public_key += e_byte_size.to_bytes(4, byteorder='big')
    public_key += e.to_bytes(e_byte_size, byteorder='big')
    # Write modulus
    n_byte_size = (n.bit_length() + 7) // 8
    public_key += n_byte_size.to_bytes(4, byteorder='big')
    public_key += n.to_bytes(n_byte_size, byteorder='big')
    if public_key_path:
        with open(public_key_path, 'wb') as f:
            f.write("ssh-rsa ".encode())
            f.write(base64.b64encode(public_key))
            print("# Created public key at `%s`" % public_key_path)

    # Write private key
    private_key = bytearray()
    # Write d
    d_byte_size = (d.bit_length() + 7) // 8
    private_key += d_byte_size.to_bytes(4, byteorder='big')
    private_key += d.to_bytes(d_byte_size, byteorder='big')
    # Write modulus
    n_byte_size = (n.bit_length() + 7) // 8
    private_key += n_byte_size.to_bytes(4, byteorder='big')
    private_key += n.to_bytes(n_byte_size, byteorder='big')
    if private_key_path:
        with open(private_key_path, 'wb') as f:
            f.write("ssh-rsa ".encode())
            f.write(base64.b64encode(private_key))
            print("# Created private key at `%s`" % private_key_path)
    return (public_key, private_key)
