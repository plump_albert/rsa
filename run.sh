#!/bin/sh

echo "$ Creating keys"
python main.py \
	--public-file key.pub \
	--private-file key \
	--generate \
	--complexity 1024
echo

echo "$ Ecrypt \`hello\` using created keys and sign encrypted file"
python main.py \
	--public-file key.pub \
	--private-file key \
	--encode \
	--sign \
	--input hello.txt \
	--output encrypted
echo

echo "$ Check signature of encrypted file"
python main.py \
	--public-file key.pub \
	--private-file key \
	--input encrypted \
	--check-sign encrypted.sig
echo

echo "$ Decrypt encrypted file"
python main.py \
	--public-file key.pub \
	--private-file key \
	--decode \
	--input encrypted \
	--output decrypted.txt
echo

echo "$ Print contents of decrypted file"
cat decrypted.txt
